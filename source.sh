# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://ftp.gnu.org/gnu/coreutils/coreutils-${BPM_PKG_VERSION}.tar.xz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  sed -e '/n_out += n_hold/,+4 s|.*bufsize.*|//&|' \
      -i src/split.c
  FORCE_UNSAFE_CONFIGURE=1 ./configure \
              --prefix=/usr            \
              --enable-no-install-program=kill,uptime
  make
}

# The check function is executed in the source directory
# This function is used to run tests to verify the package has been compiled correctly
check() {
  make check
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make DESTDIR="$BPM_OUTPUT" install
  mkdir -p "$BPM_OUTPUT"/usr/sbin/
  mv -v "$BPM_OUTPUT"/usr/bin/chroot "$BPM_OUTPUT"/usr/sbin/chroot
  mkdir -p "$BPM_OUTPUT"/usr/share/man/man8/
  mv -v "$BPM_OUTPUT"/usr/share/man/man1/chroot.1 "$BPM_OUTPUT"/usr/share/man/man8/chroot.8
  sed -i 's/"1"/"8"/' "$BPM_OUTPUT"/usr/share/man/man8/chroot.8
}
